console.log('Hello World');

/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/

//first function here:

	function userInput(){
		let name = prompt('What is your name? ');
		let age = prompt('How old are you? ');
		let address = prompt('Where do you live? ');

		console.log('Hello, ' + name);
		console.log('You are ' + age + ' years old.');
		console.log('You live in ' + address);
	}

	userInput();


/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:

	function favoriteBand(){
		let firstBand = "The Beatles";
		let secondBand = "Rihanna";
		let thirdBand = "Beyonce";
		let fourthBand = "Metallica";
		let fifthBand = "The Weekend";
		console.log('1. '+ firstBand);
		console.log('2. ' + secondBand);
		console.log('3. ' + thirdBand);
		console.log('4. ' + fourthBand);
		console.log('5. ' + fifthBand);
	}

	favoriteBand();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:

	function favoriteMovie(){
		let movie1 = ["The Godfather", '97%'];
		let movie2 = ['The Godfather, Part II', '96%'];
		let movie3 = ['Shawshank Redemption', '91%'];
		let movie4 = ['To Kill A Mockingbird', '93%'];
		let movie5 = ['Psycho', '96%'];
		console.log('1. ' + movie1[0] + '\nRotten Tomatoes Rating: ' + movie1[1]);
		console.log('2. ' + movie2[0] + '\nRotten Tomatoes Rating: ' + movie2[1]); 
		console.log('3. ' + movie3[0] + '\nRotten Tomatoes Rating: ' + movie3[1]); 
		console.log('4. ' + movie4[0] + '\nRotten Tomatoes Rating: ' + movie4[1]); 
		console.log('5. ' + movie5[0] + '\nRotten Tomatoes Rating: ' + movie5[1]); 

	}

	favoriteMovie();
/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/


let printFriends = function(){ 
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};

printFriends();


// console.log(friend1);
// console.log(friend2);